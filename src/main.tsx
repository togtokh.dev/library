import React from "react";
import ReactDOM from "react-dom/client";
import { HashRouter, Routes, Route } from "react-router-dom";
import App from "./App";
import "./index.css";
import "moki-ui/style/index.css";
import "moki-ui/style/fonts/Rubik/stylesheet.css";
import "moki-ui/style/fonts/SF-pro/stylesheet.css";
ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
  <HashRouter>
    <Routes>
      <Route path="/*" element={<App />} />
    </Routes>
  </HashRouter>
);

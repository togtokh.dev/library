import { useState, useContext, useEffect } from "react";

export default function Component() {
  const border = [8, 12, 16];
  return (
    <div className="bg-[#F5F5FA] w-full h-auto rounded-xl p-5  ">
      <div className="text-600-24">Border</div>
      <div className="w-full h-auto flex justify-between p-[5px]">
        {border.map((el) => (
          <div className="p-2 h-auto w-auto content-center mx-auto items-center flex flex-col">
            <div
              className={`h-[50px]  bg-[#ABB5BE] w-[50px] dev-border-${el}  mb-[20px]`}
            ></div>
            {el}/{`dev-border-${el}`}
          </div>
        ))}
      </div>
    </div>
  );
}

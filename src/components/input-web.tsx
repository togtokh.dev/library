import { useState, useContext, useEffect } from "react";
import MokiUI from "moki-ui";
import { svgProps } from "moki-ui/dist/cjs/components/mini-web/form/input/xl";
const addSvg = ({ handleClick }: svgProps) => {
  return (
    <div className={` dev-svg `}>
      <svg
        width="24"
        height="24"
        viewBox="0 0 24 24"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        onClick={handleClick}
      >
        <circle cx="12" cy="12" r="9" fill="#D1D8DD" />
        <circle cx="12" cy="16.25" r="0.75" fill="white" />
        <path
          d="M12 13.75C12 11.1963 14.25 12.0514 14.25 9.7404C14.25 7.03652 9.75 7.20984 9.75 9.52085"
          stroke="white"
          strokeWidth="1.5"
          stroke-miterlimit="10"
          strokeLinecap="round"
        />
      </svg>
    </div>
  );
};
export default function Component() {
  const border = [8, 12, 16];
  const [value, setValue] = useState("");
  const [status, setStatus] = useState("success");
  return (
    <div className="bg-[#F5F5FA] w-full h-auto rounded-xl p-5  ">
      <div className="text-600-24">ADMIN / input</div>
      <div className="w-full h-auto flex justify-between p-[5px] gap-6 ">
        <MokiUI.ResWeb.Form.Input.XL
          value={value}
          setValue={setValue}
          label={"Утасны дугаар"}
          placeholder=""
          type="text"
          pattern="[0-9]*(.[0-9]+)?"
          statusList={[
            {
              bgColor: "#FFFFFF",
              textColor: "#101318",
              placeholderColor: "#818E9A",
              svgFill: "#FFC800",
              caretColor: "#FFC800",
              borderColor: "#FFC800",
              status: "success",
            },
          ]}
          status={status}
          onChange={(e) => {}}
          onBlur={(e) => {}}
          onFocus={(e) => {}}
          loading={false}
          className={" dev-border-12 "}
          inputClassName="dev-border-12"
          maxLength={8}
          inputMode="numeric"
          onClick={() => {}}
          addSvg={null}
        ></MokiUI.ResWeb.Form.Input.XL>
        <MokiUI.ResWeb.Form.Input.LG
          value={value}
          setValue={setValue}
          label={"Утасны дугаар"}
          placeholder=""
          type="text"
          pattern="[0-9]*(.[0-9]+)?"
          statusList={[
            {
              bgColor: "#FFFFFF",
              textColor: "#101318",
              placeholderColor: "#818E9A",
              svgFill: "#FFC800",
              caretColor: "#FFC800",
              borderColor: "transparent",
              status: "success",
            },
          ]}
          status={status}
          onChange={(e) => {}}
          onBlur={(e) => {}}
          onFocus={(e) => {}}
          loading={true}
          className={""}
          maxLength={8}
          inputMode="numeric"
          onClick={() => {}}
          addSvg={addSvg}
        >
          <svg
            width="32"
            height="32"
            viewBox="0 0 40 40"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <rect
              opacity="0.16"
              width="40"
              height="40"
              rx="12"
              fill="#46C800"
            />
            <rect x="4" y="4" width="32" height="32" rx="10" fill="#46C800" />
            <path
              d="M20.4643 8.01165C25.2291 8.25323 29.0179 12.1931 29.0179 17.0177C29.0179 21.8423 24.9806 26.0354 20.0003 26.0354C17.5104 26.0354 15.4914 24.017 15.4914 21.5265C15.4914 19.0361 17.5098 17.0177 20.0003 17.0177C22.4907 17.0177 24.5091 14.9993 24.5091 12.5088C24.5091 10.0184 22.4902 8 20.0003 8C13.3729 8 8 13.3729 8 20.0003C8 26.6276 13.3729 32.0005 20.0003 32.0005C26.6276 32.0005 32.0005 26.6276 32.0005 20.0003C32.0005 13.3729 26.8761 8.25588 20.4643 8.01165Z"
              fill="url(#paint0_radial_687_2439)"
            />
            <path
              d="M20.4641 8.01165C20.3099 8.00583 20.1558 8 20 8C22.4899 8 24.5088 10.0184 24.5088 12.5088C24.5088 14.9993 22.4899 17.0177 20 17.0177C22.4899 17.0177 24.5088 19.0361 24.5088 21.5265C24.5088 24.017 22.4899 26.0354 20 26.0354C24.9803 26.0354 29.0177 21.998 29.0177 17.0177C29.0177 12.0373 25.2288 8.25323 20.4641 8.01165Z"
              fill="url(#paint1_linear_687_2439)"
            />
            <defs>
              <radialGradient
                id="paint0_radial_687_2439"
                cx="0"
                cy="0"
                r="1"
                gradientUnits="userSpaceOnUse"
                gradientTransform="translate(18.6838 15.6498) scale(18.9254)"
              >
                <stop offset="0.51" stopColor="white" />
                <stop offset="0.61" stopColor="#F6F8F8" />
                <stop offset="0.76" stopColor="#E0E7E6" />
                <stop offset="0.94" stopColor="#BCCCC8" />
                <stop offset="0.96" stopColor="#BACAC6" />
              </radialGradient>
              <linearGradient
                id="paint1_linear_687_2439"
                x1="15.7115"
                y1="38.122"
                x2="34.5262"
                y2="-14.9934"
                gradientUnits="userSpaceOnUse"
              >
                <stop offset="0.3" stopColor="#46C800" />
                <stop offset="0.62" stopColor="#003C28" />
              </linearGradient>
            </defs>
          </svg>
        </MokiUI.ResWeb.Form.Input.LG>
        <MokiUI.ResWeb.Form.Input.LG
          value={value}
          setValue={setValue}
          label={"Утасны дугаар"}
          placeholder=""
          type="text"
          pattern="[0-9]*(.[0-9]+)?"
          statusList={[
            {
              bgColor: "#FFFFFF",
              textColor: "#101318",
              placeholderColor: "#818E9A",
              svgFill: "#FFC800",
              caretColor: "#FFC800",
              borderColor: "transparent",
              status: "success",
            },
          ]}
          status={status}
          onChange={(e) => {}}
          onBlur={(e) => {}}
          onFocus={(e) => {}}
          loading={true}
          className={""}
          maxLength={8}
          inputMode="numeric"
          onClick={() => {}}
          addSvg={addSvg}
        ></MokiUI.ResWeb.Form.Input.LG>
        <MokiUI.ResWeb.Form.Input.LG
          value={value}
          setValue={setValue}
          label={"Утасны дугаар"}
          placeholder=""
          type="text"
          pattern="[0-9]*(.[0-9]+)?"
          statusList={[
            {
              bgColor: "#FFFFFF",
              textColor: "#101318",
              placeholderColor: "#818E9A",
              svgFill: "#FFC800",
              caretColor: "#FFC800",
              borderColor: "#101318",
              status: "success",
            },
          ]}
          status={"success"}
          onChange={(e) => {}}
          onBlur={(e) => {}}
          onFocus={(e) => {}}
          loading={true}
          className={""}
          maxLength={8}
          inputMode="numeric"
          onClick={() => {}}
          addSvg={addSvg}
        ></MokiUI.ResWeb.Form.Input.LG>
      </div>
    </div>
  );
}

import { useState, useContext, useEffect } from "react";

export default function Component() {
  const shadow = [4, 8, 16, 32];
  return (
    <div className="bg-[#F5F5FA] w-full h-auto rounded-xl p-5  ">
      <div className="text-600-24">Shadow</div>
      <div className="w-full h-auto flex justify-between p-[5px]">
        {shadow.map((el) => (
          <div className="p-2 h-auto w-auto content-center mx-auto items-center flex flex-col">
            <div
              className={`h-[50px]  bg-white w-[50px] rounded-xl dev-shadow-${el} mb-[20px]`}
            ></div>
            {el}/{`dev-shadow-${el}`}
          </div>
        ))}
      </div>
    </div>
  );
}

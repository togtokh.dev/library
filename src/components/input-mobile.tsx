import { useState, useContext, useEffect } from "react";
import MokiUI, { Input } from "moki-ui";
import { svgProps } from "moki-ui/dist/cjs/components/mini-web/form/input/xl";
const addSvg = ({ handleClick }: svgProps) => {
  return (
    <div className={` dev-svg `}>
      <svg
        width="24"
        height="24"
        viewBox="0 0 24 24"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        onClick={handleClick}
      >
        <circle cx="12" cy="12" r="9" fill="#D1D8DD" />
        <circle cx="12" cy="16.25" r="0.75" fill="white" />
        <path
          d="M12 13.75C12 11.1963 14.25 12.0514 14.25 9.7404C14.25 7.03652 9.75 7.20984 9.75 9.52085"
          stroke="white"
          strokeWidth="1.5"
          stroke-miterlimit="10"
          strokeLinecap="round"
        />
      </svg>
    </div>
  );
};
const addEdSvg = ({ handleClick }: svgProps) => {
  return (
    <svg
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      className={` dev-svg `}
    >
      <path
        d="M19.9898 18.9533C20.5469 18.9533 21 19.4123 21 19.9766C21 20.5421 20.5469 21 19.9898 21H14.2797C13.7226 21 13.2695 20.5421 13.2695 19.9766C13.2695 19.4123 13.7226 18.9533 14.2797 18.9533H19.9898ZM16.0299 3.69906L17.5049 4.87078C18.1097 5.34377 18.513 5.96726 18.6509 6.62299C18.8101 7.3443 18.6403 8.0527 18.1628 8.66544L9.3764 20.0279C8.97316 20.5439 8.37891 20.8341 7.74222 20.8449L4.24039 20.8879C4.04939 20.8879 3.89021 20.7589 3.84777 20.5761L3.0519 17.1255C2.91395 16.4912 3.0519 15.8355 3.45514 15.3303L9.68413 7.26797C9.79025 7.13898 9.98126 7.11855 10.1086 7.21422L12.7297 9.29967C12.8994 9.43942 13.1329 9.51467 13.377 9.48242C13.8969 9.41792 14.2471 8.94493 14.1941 8.43969C14.1622 8.1817 14.0349 7.96671 13.8651 7.80546C13.812 7.76246 11.3183 5.76301 11.3183 5.76301C11.1591 5.63401 11.1273 5.39752 11.2546 5.23735L12.2415 3.95706C13.1541 2.78534 14.7459 2.67784 16.0299 3.69906Z"
        fill="#D1D8DD"
      />
    </svg>
  );
};
export default function Component() {
  const border = [8, 12, 16];
  const [value, setValue] = useState("");
  const [value2, setValue2] = useState("");
  const [value3, setValue3] = useState("");
  const [status, setStatus] = useState("success");
  return (
    <>
      <div className="bg-[#F5F5FA] w-full h-auto rounded-xl p-5  ">
        <div className="text-600-24">Input</div>
        <div className="w-full h-auto flex flex-col justify-between p-[5px] gap-6 ">
          <div className=" flex flex-col">
            <Input.XL
              value={value}
              setValue={setValue}
              label={"Утасны дугаар"}
              placeholder=""
              type="text"
              pattern="[0-9]*(.[0-9]+)?"
              statusList={[
                {
                  bgColor: "#FFFFFF",
                  textColor: "#101318",
                  placeholderColor: "#818E9A",
                  svgFill: "#FFC800",
                  caretColor: "#FFC800",
                  borderColor: "#FFC800",
                  status: "success",
                },
              ]}
              status={status}
              onChange={(e) => {}}
              onBlur={(e) => {}}
              onFocus={(e) => {}}
              loading={false}
              className={" dev-border-12 "}
              inputClassName={" dev-border-12"}
              svgClassName={" bg-red-300 "}
              maxLength={8}
              inputMode="numeric"
              onClick={() => {}}
              addSvg={null}
            >
              <div className="h-[40px] w-[40px] flex justify-center items-center">
                <svg
                  width="24"
                  height="24"
                  viewBox="0 0 24 24"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    fillRule="evenodd"
                    clipRule="evenodd"
                    d="M12.0005 11.75C14.4857 11.75 16.5 9.73473 16.5 7.24951C16.5 4.76429 14.4857 2.75 12.0005 2.75C9.51527 2.75 7.5 4.76429 7.5 7.24951C7.49163 9.72634 9.5246 11.7416 12.0005 11.75Z"
                    stroke="#818E9A"
                    strokeWidth="1.5"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                  />
                  <path
                    fillRule="evenodd"
                    clipRule="evenodd"
                    d="M12.0005 15.25C8.22455 15.25 5 15.8473 5 18.2393C5 20.6313 8.20409 21.25 12.0005 21.25C15.7764 21.25 19 20.6518 19 18.2607C19 15.8696 15.7968 15.25 12.0005 15.25Z"
                    stroke="#818E9A"
                    strokeWidth="1.5"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                  />
                </svg>
              </div>
            </Input.XL>
            <div className="mt-7"></div>
            <Input.XL
              value={value2}
              setValue={setValue2}
              label={"Утасны дугаар"}
              placeholder=""
              type="text"
              pattern="[0-9]*(.[0-9]+)?"
              statusList={[
                {
                  bgColor: "#FFFFFF",
                  textColor: "#E03B39",
                  placeholderColor: "#818E9A",
                  svgFill: "#E03B39",
                  caretColor: "#E03B39",
                  borderColor: "#E03B39",
                  status: "success",
                },
              ]}
              status={status}
              onChange={(e) => {}}
              onBlur={(e) => {}}
              onFocus={(e) => {}}
              loading={false}
              className={" dev-border-12 "}
              inputClassName={" dev-border-12"}
              svgClassName={" "}
              maxLength={8}
              inputMode="numeric"
              onClick={() => {}}
              addSvg={null}
            >
              <div className="h-[40px] w-[40px] flex justify-center items-center">
                <svg
                  width="24"
                  height="24"
                  viewBox="0 0 24 24"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    fillRule="evenodd"
                    clipRule="evenodd"
                    d="M12.0005 11.75C14.4857 11.75 16.5 9.73473 16.5 7.24951C16.5 4.76429 14.4857 2.75 12.0005 2.75C9.51527 2.75 7.5 4.76429 7.5 7.24951C7.49163 9.72634 9.5246 11.7416 12.0005 11.75Z"
                    stroke="#818E9A"
                    strokeWidth="1.5"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                  />
                  <path
                    fillRule="evenodd"
                    clipRule="evenodd"
                    d="M12.0005 15.25C8.22455 15.25 5 15.8473 5 18.2393C5 20.6313 8.20409 21.25 12.0005 21.25C15.7764 21.25 19 20.6518 19 18.2607C19 15.8696 15.7968 15.25 12.0005 15.25Z"
                    stroke="#818E9A"
                    strokeWidth="1.5"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                  />
                </svg>
              </div>
            </Input.XL>
            <div className="mt-7"></div>
            <Input.XL
              value={value3}
              setValue={setValue3}
              label={"Утасны дугаар"}
              placeholder=""
              type="text"
              pattern="[0-9]*(.[0-9]+)?"
              statusList={[
                {
                  bgColor: "#FFFFFF",
                  textColor: "#43D365",
                  placeholderColor: "#818E9A",
                  svgFill: "#43D365",
                  caretColor: "#43D365",
                  borderColor: "#43D365",
                  status: "success",
                },
              ]}
              status={status}
              onChange={(e) => {}}
              onBlur={(e) => {}}
              onFocus={(e) => {}}
              loading={false}
              className={" dev-border-12 "}
              inputClassName={" dev-border-12"}
              svgClassName={" "}
              maxLength={8}
              inputMode="numeric"
              onClick={() => {}}
              addSvg={null}
            >
              <div className="h-[40px] w-[40px] flex justify-center items-center">
                <svg
                  width="24"
                  height="24"
                  viewBox="0 0 24 24"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    fillRule="evenodd"
                    clipRule="evenodd"
                    d="M12.0005 11.75C14.4857 11.75 16.5 9.73473 16.5 7.24951C16.5 4.76429 14.4857 2.75 12.0005 2.75C9.51527 2.75 7.5 4.76429 7.5 7.24951C7.49163 9.72634 9.5246 11.7416 12.0005 11.75Z"
                    stroke="#818E9A"
                    strokeWidth="1.5"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                  />
                  <path
                    fillRule="evenodd"
                    clipRule="evenodd"
                    d="M12.0005 15.25C8.22455 15.25 5 15.8473 5 18.2393C5 20.6313 8.20409 21.25 12.0005 21.25C15.7764 21.25 19 20.6518 19 18.2607C19 15.8696 15.7968 15.25 12.0005 15.25Z"
                    stroke="#818E9A"
                    strokeWidth="1.5"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                  />
                </svg>
              </div>
            </Input.XL>
          </div>
          <div className=" flex flex-col">
            <Input.XL
              value={value}
              setValue={setValue}
              label={"Нууц үг"}
              placeholder=""
              type="password"
              //  pattern="[0-9]*(.[0-9]+)?"
              statusList={[
                {
                  bgColor: "#FFFFFF",
                  textColor: "#101318",
                  placeholderColor: "#818E9A",
                  svgFill: "#FFC800",
                  caretColor: "#FFC800",
                  borderColor: "#FFC800",
                  status: "success",
                },
              ]}
              status={status}
              onChange={(e) => {}}
              onBlur={(e) => {}}
              onFocus={(e) => {}}
              loading={false}
              className={" dev-border-12 "}
              inputClassName={" dev-border-12"}
              svgClassName={" "}
              maxLength={8}
              inputMode="numeric"
              onClick={() => {}}
              addSvg={addSvg}
            ></Input.XL>
            <div className="mt-7"></div>
            <Input.XL
              value={value}
              setValue={setValue}
              label={"Нэвтрэх нууц үг 1"}
              placeholder=""
              type="text"
              pattern="[0-9]*(.[0-9]+)?"
              statusList={[
                {
                  bgColor: "#FFFFFF",
                  textColor: "#101318",
                  placeholderColor: "#818E9A",
                  svgFill: "#FFC800",
                  caretColor: "#FFC800",
                  borderColor: "#FFC800",
                  status: "success",
                },
              ]}
              status={status}
              onChange={(e) => {}}
              onBlur={(e) => {}}
              onFocus={(e) => {}}
              loading={true}
              className={" rounded-l-2xl "}
              inputClassName={" rounded-l-2xl "}
              svgClassName={" "}
              // maxLength={4}
              inputMode="numeric"
              onClick={() => {}}
              addSvg={addSvg}
            >
              <svg
                width="40"
                height="40"
                viewBox="0 0 40 40"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <rect
                  opacity="0.16"
                  width="40"
                  height="40"
                  rx="12"
                  fill="#46C800"
                />
                <rect
                  x="4"
                  y="4"
                  width="32"
                  height="32"
                  rx="10"
                  fill="#46C800"
                />
                <path
                  d="M20.4643 8.01165C25.2291 8.25323 29.0179 12.1931 29.0179 17.0177C29.0179 21.8423 24.9806 26.0354 20.0003 26.0354C17.5104 26.0354 15.4914 24.017 15.4914 21.5265C15.4914 19.0361 17.5098 17.0177 20.0003 17.0177C22.4907 17.0177 24.5091 14.9993 24.5091 12.5088C24.5091 10.0184 22.4902 8 20.0003 8C13.3729 8 8 13.3729 8 20.0003C8 26.6276 13.3729 32.0005 20.0003 32.0005C26.6276 32.0005 32.0005 26.6276 32.0005 20.0003C32.0005 13.3729 26.8761 8.25588 20.4643 8.01165Z"
                  fill="url(#paint0_radial_687_2439)"
                />
                <path
                  d="M20.4641 8.01165C20.3099 8.00583 20.1558 8 20 8C22.4899 8 24.5088 10.0184 24.5088 12.5088C24.5088 14.9993 22.4899 17.0177 20 17.0177C22.4899 17.0177 24.5088 19.0361 24.5088 21.5265C24.5088 24.017 22.4899 26.0354 20 26.0354C24.9803 26.0354 29.0177 21.998 29.0177 17.0177C29.0177 12.0373 25.2288 8.25323 20.4641 8.01165Z"
                  fill="url(#paint1_linear_687_2439)"
                />
                <defs>
                  <radialGradient
                    id="paint0_radial_687_2439"
                    cx="0"
                    cy="0"
                    r="1"
                    gradientUnits="userSpaceOnUse"
                    gradientTransform="translate(18.6838 15.6498) scale(18.9254)"
                  >
                    <stop offset="0.51" stopColor="white" />
                    <stop offset="0.61" stopColor="#F6F8F8" />
                    <stop offset="0.76" stopColor="#E0E7E6" />
                    <stop offset="0.94" stopColor="#BCCCC8" />
                    <stop offset="0.96" stopColor="#BACAC6" />
                  </radialGradient>
                  <linearGradient
                    id="paint1_linear_687_2439"
                    x1="15.7115"
                    y1="38.122"
                    x2="34.5262"
                    y2="-14.9934"
                    gradientUnits="userSpaceOnUse"
                  >
                    <stop offset="0.3" stopColor="#46C800" />
                    <stop offset="0.62" stopColor="#003C28" />
                  </linearGradient>
                </defs>
              </svg>
            </Input.XL>
            <div className="mt-7"></div>
            <Input.XL
              value={value}
              setValue={setValue}
              label={"Нэвтрэх нууц үг 22"}
              placeholder=""
              type="text"
              pattern="[0-9]*(.[0-9]+)?"
              statusList={[
                {
                  bgColor: "#FFFFFF",
                  textColor: "#101318",
                  placeholderColor: "#818E9A",
                  svgFill: "#FFC800",
                  caretColor: "#FFC800",
                  borderColor: "#FFC800",
                  status: "success",
                },
              ]}
              status={status}
              onChange={(e) => {}}
              onBlur={(e) => {}}
              onFocus={(e) => {}}
              loading={false}
              className={" dev-border-12 "}
              inputClassName={" dev-border-12"}
              svgClassName={" "}
              // maxLength={4}
              inputMode="numeric"
              onClick={() => {}}
              addSvg={addEdSvg}
            ></Input.XL>
          </div>
        </div>
      </div>
    </>
  );
}

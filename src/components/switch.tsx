import { useState, useContext, useEffect } from "react";
import MokiUI from "moki-ui";
export default function Component() {
  const shadow = [4, 8, 16, 32];
  const [value, setValue] = useState(false);
  return (
    <div className="bg-[#F5F5FA] w-full h-auto rounded-xl p-5  ">
      <div className="text-600-24">Switch</div>
      <div className="w-full h-auto flex justify-between p-[5px]">
        <div className="p-2 h-auto w-auto content-center mx-auto items-center flex flex-col">
          {value ? "true" : "false"}
          <MokiUI.MiniWeb.Form.Switch.XL
            isOn={value}
            handleToggle={() => {
              setValue(!value);
            }}
            className=""
            activeColor="#CBFF35"
            inactiveColor="#CBFF35"
          />
        </div>
        <div className="p-2 h-auto w-auto content-center mx-auto items-center flex flex-col">
          {value ? "true" : "false"}
          <MokiUI.MiniWeb.Form.Switch.LG
            isOn={value}
            handleToggle={() => {
              setValue(!value);
            }}
            className=""
            activeColor="#CBFF35"
            inactiveColor="#CBFF35"
          />
        </div>
        {/* {shadow.map((el) => (
          <div className="p-2 h-auto w-auto content-center mx-auto items-center flex flex-col">
            <div
              className={`h-[50px]  bg-white w-[50px] rounded-xl dev-shadow-${el} mb-[20px]`}
            ></div>
            {el}/{`dev-shadow-${el}`}
          </div>
        ))} */}
      </div>
    </div>
  );
}

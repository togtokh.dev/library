import { useState, useContext, useEffect } from "react";
export default function Component() {
  const size = [48, 36, 24, 20, 16, 14, 12, 10, 8];
  const fonts = [400, 500, 600];
  return (
    <div className="bg-[#F5F5FA] w-full h-auto rounded-xl p-5">
      <div className="text-600-24">Fonts</div>
      {size.map((e) => (
        <div className="w-full h-auto flex justify-between p-[5px]">
          {fonts.map((el) => (
            <>
              <div className={`w-[33%] text-${el}-${e}`}>
                {el}/{`text-${el}-${e}`}
              </div>
            </>
          ))}
        </div>
      ))}
    </div>
  );
}

import { useState, useContext, useEffect } from "react";
import MokiUI, { ButtonXL, ButtonSM, ButtonLG, ButtonMD } from "moki-ui";
export default function Component() {
  return (
    <div className="bg-[#F5F5FA] w-full h-auto rounded-xl p-5 mt-[20px]">
      <div className="text-600-24">Button</div>

      <div className="w-full h-auto flex justify-between p-[5px] gap-6 ">
        <div className="w-[25%] flex flex-col gap-[16px]">
          <ButtonXL
            className="bg-system-color text-system-light dark:text-system-dark dev-border-12 w-full disabled:text-lable-secondary-light disabled:bg-fill-primary-light dark:disabled:text-lable-secondary-dark  dark:disabled:bg-fill-primary-dark active:btn-hover hover:btn-hover dev-scale"
            type="submit"
            onClick={() => {}}
          >
            Button XL
          </ButtonXL>
          <ButtonXL
            className="bg-system-color text-system-light dark:text-system-dark dev-border-12 w-full disabled:text-lable-secondary-light disabled:bg-fill-primary-light dark:disabled:text-lable-secondary-dark  dark:disabled:bg-fill-primary-dark active:btn-hover hover:btn-hover "
            type="submit"
            disabled={false}
            onClick={() => {}}
          >
            Button XL
          </ButtonXL>
          <ButtonXL
            className="bg-system-color text-system-light dark:text-system-dark dev-border-12 w-full disabled:text-lable-secondary-light disabled:bg-fill-primary-light dark:disabled:text-lable-secondary-dark  dark:disabled:bg-fill-primary-dark active:btn-hover hover:btn-hover "
            backgroundColor="#FFC908"
            type="submit"
            disabled={true}
            onClick={() => {}}
          >
            Button XL
          </ButtonXL>
        </div>
        <div className="w-[25%] flex flex-col gap-[16px]">
          <ButtonLG
            className="bg-system-color text-system-light dark:text-system-dark dev-border-12 w-full disabled:text-lable-secondary-light disabled:bg-fill-primary-light dark:disabled:text-lable-secondary-dark  dark:disabled:bg-fill-primary-dark active:btn-hover hover:btn-hover "
            backgroundColor="#FFC908"
            type="submit"
            disabled={false}
            onClick={() => {}}
          >
            Button LG
          </ButtonLG>
          <ButtonLG
            className="bg-system-color text-system-light dark:text-system-dark dev-border-12 w-full disabled:text-lable-secondary-light disabled:bg-fill-primary-light dark:disabled:text-lable-secondary-dark  dark:disabled:bg-fill-primary-dark active:btn-hover hover:btn-hover "
            backgroundColor="#FFC908"
            type="submit"
            disabled={false}
            onClick={() => {}}
          >
            Button LG
          </ButtonLG>
          <ButtonLG
            className="bg-system-color text-system-light dark:text-system-dark dev-border-12 w-full disabled:text-lable-secondary-light disabled:bg-fill-primary-light dark:disabled:text-lable-secondary-dark  dark:disabled:bg-fill-primary-dark active:btn-hover hover:btn-hover "
            backgroundColor="#FFC908"
            type="submit"
            disabled={true}
            onClick={() => {}}
          >
            Button LG
          </ButtonLG>
        </div>
        <div className="w-[25%] flex flex-col gap-[16px]">
          <ButtonMD
            className="bg-system-color text-system-light dark:text-system-dark dev-border-12 w-full disabled:text-lable-secondary-light disabled:bg-fill-primary-light dark:disabled:text-lable-secondary-dark  dark:disabled:bg-fill-primary-dark active:btn-hover hover:btn-hover "
            backgroundColor="#FFC908"
            type="submit"
            disabled={false}
            onClick={() => {}}
          >
            Button MD
          </ButtonMD>
          <ButtonMD
            className="bg-system-color text-system-light dark:text-system-dark dev-border-12 w-full disabled:text-lable-secondary-light disabled:bg-fill-primary-light dark:disabled:text-lable-secondary-dark  dark:disabled:bg-fill-primary-dark active:btn-hover hover:btn-hover "
            backgroundColor="#FFC908"
            type="submit"
            disabled={false}
            onClick={() => {}}
          >
            Button MD
          </ButtonMD>
          <ButtonMD
            className="bg-system-color text-system-light dark:text-system-dark dev-border-12 w-full disabled:text-lable-secondary-light disabled:bg-fill-primary-light dark:disabled:text-lable-secondary-dark  dark:disabled:bg-fill-primary-dark active:btn-hover hover:btn-hover "
            backgroundColor="#FFC908"
            type="submit"
            disabled={true}
            onClick={() => {}}
          >
            Button MD
          </ButtonMD>
        </div>
        <div className="w-[25%] flex flex-col gap-[16px]">
          <ButtonSM
            className="bg-system-color text-system-light dark:text-system-dark dev-border-12 w-full disabled:text-lable-secondary-light disabled:bg-fill-primary-light dark:disabled:text-lable-secondary-dark  dark:disabled:bg-fill-primary-dark active:btn-hover hover:btn-hover "
            backgroundColor="#FFC908"
            type="submit"
            disabled={false}
            onClick={() => {}}
          >
            Button SM
          </ButtonSM>
          <ButtonSM
            className="bg-system-color text-system-light dark:text-system-dark dev-border-12 w-full disabled:text-lable-secondary-light disabled:bg-fill-primary-light dark:disabled:text-lable-secondary-dark  dark:disabled:bg-fill-primary-dark active:btn-hover hover:btn-hover "
            backgroundColor="#FFC908"
            type="submit"
            disabled={false}
            onClick={() => {}}
          >
            Button SM
          </ButtonSM>
          <ButtonSM
            className="bg-system-color text-system-light dark:text-system-dark dev-border-12 w-full disabled:text-lable-secondary-light disabled:bg-fill-primary-light dark:disabled:text-lable-secondary-dark  dark:disabled:bg-fill-primary-dark active:btn-hover hover:btn-hover "
            backgroundColor="#FFC908"
            type="submit"
            disabled={true}
            onClick={() => {}}
          >
            Button SM
          </ButtonSM>
        </div>
      </div>
    </div>
  );
}

import { useCallback, useState, useContext, useEffect } from "react";
import {
  Fonts,
  Shadow,
  Button,
  Border,
  Input,
  InputWeb,
  InputMobile,
  Switch,
} from "src/components";
function App() {
  return (
    <div className=" h-full w-full p-10">
      <InputMobile />
      <div className="mt-[20px]"></div>
      <Fonts />
      <div className="mt-[20px]"></div>
      <Shadow />
      <div className="mt-[20px]"></div>
      <Border />
      <div className="mt-[20px]"></div>
      <Button />
      <div className="mt-[20px]"></div>
      <Input />
      <div className="mt-[20px]"></div>
      <InputWeb />
      <div className="mt-[20px] "></div>
      <Switch />
      {/* <MokiUI.MiniWeb.Button.Default
        className=" bg-[#1BB683] rounded-[12px] text-white text-xl-16 mb-[16px]"
        type="submit"
        disabled={false}
        loading={false}
        handleClick={() => {}}
      >
        Нэвтрэх
      </MokiUI.MiniWeb.Button.Default> */}
    </div>
  );
}

export default App;
